using System.Collections.ObjectModel;

namespace myHorloge
{
    class FuseauHoraire
    {
        public TimeZoneInfo Tzi {get; set;}
        public String NomVille {get; set;}
        public TimeSpan Offset {get; }

        public FuseauHoraire(TimeZoneInfo tzi, String nomVille)
        {
            Tzi = tzi;
            NomVille = nomVille;
            Offset = Tzi.BaseUtcOffset; // Calculer le décalage horaire ici
        }
        public void ShFuseau()
        {
            DateTime Gmt = DateTime.UtcNow;
            Gmt = Gmt + Offset;
            string heure = Gmt.ToString("HH:mm:ss");
            string decalage = string.Format("{0}{1:D2}:{2:D2}", (Offset < TimeSpan.Zero ? "-" : "+"), 
                                              Math.Abs(Offset.Hours), Math.Abs(Offset.Minutes));
            Console.WriteLine($"{NomVille} - {heure}\nUTC {decalage}");
            Gmt = Gmt - Offset;
        }
    }
    class Horloge
    {
        public string NomVille {get; set;}
        public DateTime HeureVille {get; set;}
        public int DecalageHoraire {get; set;}

        public Horloge(string nomVille,DateTime heureCourante, int decalageHoraire)
        {
            this.NomVille = nomVille;
            this.HeureVille = heureCourante;
            this.DecalageHoraire = decalageHoraire;
        }
        public static String DateNow()
        {
            DateTime dateCourante = DateTime.Now;    //Date a l'instant
            return dateCourante.ToString("HH:mm:ss  \nANTANANARIVO \nddd dd MMMM");
        }
        public static void changeLine(string setString, int numOfLine)
        {
            int CursorLeft = Console.CursorLeft;
            int CursorTop = Console.CursorTop;
            Console.SetCursorPosition(0, CursorTop - numOfLine);

            Console.Write(setString);

            Console.SetCursorPosition(CursorLeft, CursorTop);
        }
    }
}