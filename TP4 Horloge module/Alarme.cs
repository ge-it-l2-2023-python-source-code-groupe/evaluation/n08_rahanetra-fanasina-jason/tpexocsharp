namespace AlarmClass
{
    class Alarme
    {
        //Attribuer aux different alarmes

        public string NomAlarme {get; set;}
        public string Time {get;set;}
        public string Date {get;set;}
        public string JourAlarme {get;set;}
        public string Periodique {get; set;}
        public static string jour="\n";

        static List<String> charValid = new List<String>{"L","M","Me","J","V","S","D"};


        //Constructeurs
        public  Alarme (string nomAlarme, string timeAlarme,string periodicite, string dateAlarme, string jourAlarme)
        {
            this.NomAlarme = nomAlarme;
            this.Time = timeAlarme;
            this.Periodique = periodicite;
            this.Date = dateAlarme;
            this.JourAlarme =jourAlarme;
        }
        public void VoirLesAlarmes()
        {
            Console.WriteLine($"{NomAlarme} - {Time} {Periodique} \n{JourAlarme} {Date}");
            // Liste d’alarme actif:
            // Mon réveil - 06:30 (périodique)
            // Lun., mard., merc., jeu., vend
        }

        public static void AjouterJour()
        {
            string jourChar = "";
            while(!charValid.Contains(jourChar))
            {
                Console.Write("Jour de planification (L/M/ME/J/V/S/D): ");
                jourChar = Console.ReadLine()??"";
            }
            switch (jourChar)
            {
                case "L":
                    jour += "Lundi., ";
                    break;  
                case "M":
                    jour += "Mardi., ";
                    break;
                case "Me":
                    jour += "Mercredi., ";
                    break;
                case "J":
                    jour += "Jeudi., ";
                    break;
                case "V":
                    jour += "Vendredi., ";
                    break;
                case "S":
                    jour += "Samedi., ";
                    break;
                case "D":
                    jour += "Dimanche., ";
                    break;
            }
        }
    }
}