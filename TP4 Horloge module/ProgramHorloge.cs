﻿using System.Xml;
using AlarmClass;
using myHorloge;
namespace Horloge_module;
class ProgramHorloge
{
    public static void MainHorloge()
    {   
        // obtenir tous les fuseaux horaire dispo sur la machine

        var fuseauxHoraires = TimeZoneInfo.GetSystemTimeZones(); 
        List<Alarme> ListAlarme = new List<Alarme>();
        List<FuseauHoraire> ListFuseau = new List<FuseauHoraire>();

        List<String> JourValid = new List<string>{"Lundi","Mardi","Mercredi","Jeudi",
                                                  "Vendredi","Samedi","Dimanche"};

        string choice = "1";
        string option = "1";

        while(choice!="3")
        {
            Console.Clear();
            Console.WriteLine("🕘🕘🕘🕘🕘 HORLOGE MODULE 🕘🕘🕘🕘🕘"+
                            "\n\nChoisissez une option :"+
                            "\n 1 - Alarme"+
                            "\n 2 - Horloge"+
                            "\n 3 - Quiter"+
                            "\n\n Quel est votre choix ? "); 
            choice = Console.ReadLine()??"";
            Console.Clear();

            switch (choice)
            {
                // POUR LE MENU ALARME----------------------------------------------------------------------------------------------------

                case "1":
                    while(option!="3")
                    {
                        Console.WriteLine("⏰⏰⏰⏰⏰ ALARME ⏰⏰⏰⏰⏰"+
                            "\n\nVoici le menu :"+
                            "\n 1 - Voir les Alarme actif"+
                            "\n 2 - Creer une alarme"+
                            "\n 3 - Retour menu principale"+
                            "\n\n Quel est votre choix ? "); 
                        option = Console.ReadLine()??"";
                        Console.ReadKey();
                        Console.Clear();

                        if(option == "1")
                        {
                            Console.WriteLine("Liste d’alarme actif: \n");
                            if (ListAlarme.Count()>0)
                            {
                                foreach (Alarme alarme in ListAlarme)
                                {
                                    alarme.VoirLesAlarmes();
                                    Console.WriteLine("\n");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Il n'y a aucun horloge pour le moment 😓");
                            }
                            
                            Console.ReadKey();
                            Console.Clear();
                        }
                        else if(option == "2")
                        {
                            //Entrer les differentes informations sur l'alarme

                            Console.Write("Info de la nouvelle Alarme  \n"+
                                          "\nDonnez un nom de référence: ");
                            string nom = Console.ReadLine()??"";
                            while (nom == "")
                            {
                                Console.Write("Info de la nouvelle Alarme  \n"+
                                            "\nDonnez un nom de référence s'il vous plait😕: ");
                                nom = Console.ReadLine()??"";
                            }

                            Console.Write("Heure de l’alarme sous ce format (hh:mm): ");
                            string heure = Console.ReadLine()??"";
                            while (!heure.Contains(':') || heure =="")
                            {
                                Console.Write("!!!Veuillez respecter le format s'il vous plait\n!!!"+
                                              "Heure de l’alarme sous ce format (hh:mm)😕: ");
                                heure = Console.ReadLine()??"";                               
                            }

                            Console.Write("Périodique (y/n) : ");
                            string choix = Console.ReadLine()??"";
                            while (choix != "y" && choix != "n")
                            {
                                Console.Write("!!!Périodique (y/n) 😕: ");
                                choix = Console.ReadLine()??"";
                            }

                            //Declaration et initialisation des variables a utiliser
                            
                            string date="";
                            string periodicite = "";

                            // POUR ALARME NON PERIODIQUE  

                            if (choix == "n")
                            {
                                periodicite = "";
                                Alarme.AjouterJour();  //APPEL DE METHODE AjouterJour Pour un ajout jour particulier
                                Console.WriteLine("Lancer une seule fois (y/n): ");
                                choice = Console.ReadLine()??"";
                                while (choice != "y" && choice != "n")
                                {
                                    Console.Write("Lancer une seule fois (y/n)😕: ");
                                    choix = Console.ReadLine()??"";
                                }
                                

                                if(choice == "y")
                                {
                                    Console.Write("Entrer la date precise sous cet format (yyyy-MM-dd) : ");
                                    date = Console.ReadLine()??""; // ajouter une date option
                                    while (!date.Contains('-') || date =="")
                                    {
                                        Console.Write("!!!Veuillez respecter le format s'il vous plait\n!!!"+
                                                    "Entrer la date precise sous cet format (yyyy-MM-dd) 😕: ");
                                        date = Console.ReadLine()??"";                               
                                    }
                                }
                            }
                            
                            // POUR ALARME PERDIODIQUE

                            else if (choix == "y")
                            {   
                                periodicite = "(periodique)";
                                Alarme.AjouterJour();   //APPEL DE METHODE AjouterJour Pour un ajout jour particulier
                                string repetition = "y";

                                while(repetition != "n")
                                { 
                                    Console.Write("Ajouter un autre jour ? 🤔(y/n) ");
                                    repetition = Console.ReadLine()??"";

                                    if(repetition == "y")
                                    {
                                        Alarme.AjouterJour();
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }

                            // creer l'alarme avec la classe Alarme

                            Alarme alarme = new Alarme(nom,heure,periodicite,date,Alarme.jour);
                            ListAlarme.Add(alarme);     //AJOUTER L'INSTANCE Alarme AU ListAlarme
                            Alarme.jour = "";    //REINITIALISER A VIDE L'ATTRIBUT jour du classe ALarme

                            //MODE DE L'ALARME

                            Console.WriteLine("Activer sonnerie par defaut (y/n) ");
                            string mode = Console.ReadLine()??"";

                            while (mode != "y" && mode != "n")
                            {
                                Console.WriteLine("!!!Activer sonnerie par defaut 😕 (y/n)  ");
                                mode = Console.ReadLine()??"";
                            }
                            if (mode == "y")
                            {
                                Console.WriteLine("Votre alarme est active par defaut 😊");
                            }
                            else
                            {
                                Console.WriteLine("Votre alarme n'est pas active par defaut 😊");
                            }
                            Console.WriteLine("\n✨✨✨ Votre nouvelle alarme est enregistrer ✨✨✨😁");
                            
                            Console.ReadKey();
                            Console.Clear();
                        }
                    }
                    break;
                
                // POUR LE MENU HORLOGE-----------------------------------------------------------------------------------------------

                case "2":
                    option = "1";
                    while(option!="3")
                    {   
                        Console.WriteLine(Horloge.DateNow());
                        Console.WriteLine("\n⌚⌚⌚⌚⌚ HORLOGE MODULE ⌚⌚⌚⌚⌚"+
                            "\nVoici le menu :"+
                            "\n 1 - Voir les Horloges actifs"+
                            "\n 2 - Ajouter une Horloge"+
                            "\n 3 - Retour menu principale"+
                            "\n\n Quel est votre choix ? "); 
                        option = Console.ReadLine()??"";
                        Console.ReadKey();
                        Console.Clear();

                        if(option!="")
                        {
                             switch(option)
                            {
                                //VOIR LES HORLOGES ACTIFS
                                case "1":
                                    Console.WriteLine("🕘🕘🕘Liste d’Horloge : 🕘🕘🕘\n");

                                    //AFFICHER LES HORLOGES ACTIFS
                                    if (ListFuseau.Count()>0)
                                    {
                                        foreach (FuseauHoraire fuseau in ListFuseau)
                                        {                                     
                                            fuseau.ShFuseau();
                                        }                                       
                                    }
                                    else 
                                    {
                                        Console.WriteLine("Il n'y a aucun horloge pour le moment 😓");
                                    }
                                    break;
                                //AJOUTER UNE HORLOGE
                                case "2":
                                    int i = 1;

                                    //AJOUTER UNE HORLOGE
                                    bool found = false;

                                    while (found != true)
                                    {
                                        foreach (TimeZoneInfo lesFuseaux in fuseauxHoraires)
                                        {
                                            Console.WriteLine($"{i}" + lesFuseaux);
                                        }

                                        Console.WriteLine("\nTaper un mot cle pour pouvoir rechercher une ville ou un pays : ");
                                        string wordToSearch = Console.ReadLine() ?? "";
                                        while(wordToSearch=="")
                                        {
                                            Console.WriteLine("\n!!!Veuillez Taper un mot cle pour pouvoir rechercher une ville ou un pays 😕: ");
                                            wordToSearch = Console.ReadLine() ?? "";
                                        }
                                        wordToSearch = wordToSearch.ToLowerInvariant();

                                        foreach (TimeZoneInfo fuseauHoraire in fuseauxHoraires)
                                        {
                                            if (fuseauHoraire.DisplayName.ToLowerInvariant().Contains(wordToSearch) ||
                                                fuseauHoraire.Id.ToLowerInvariant().Contains(wordToSearch))
                                            {
                                                Console.WriteLine($"L'élément correspondant à '{wordToSearch}' est : {fuseauHoraire}");
                                                Console.WriteLine("Voulez-vous enregistrer (y/n) : ");
                                                option = Console.ReadLine()??"";
                                                if(option == "y")
                                                {    
                                                    Console.WriteLine("✨✨✨✨ Votre alarme a bien ete enregistre ✨✨✨✨ 😌");
                                                    FuseauHoraire fuseaux = new FuseauHoraire(fuseauHoraire,wordToSearch);
                                                    ListFuseau.Add(fuseaux);
                                                }
                                                found = true;        
                                                break;
                                            }
                                            i++;
                                        }

                                        if (!found)
                                        {
                                            Console.WriteLine($"Aucun élément correspondant à '{wordToSearch}' n'a été trouvé.Veuillez"+
                                                               "retaper quelques chose d'autre (Pays,..)");
                                        }
                                    }
                                    
                                    break;
                                case "3":
                                    break;
                            }
                        }
                        Console.ReadKey();
                        Console.Clear();
                    }
                    break;

                // POUR QUITTER------------------------------------------------------------------------------------------------------------

                case "3":
                    break;
            }         
        }
    }
}
