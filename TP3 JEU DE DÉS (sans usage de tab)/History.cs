namespace HistoryGame
{
    public static class History
    {
        public static String[] history = new String[]{};
        public static String historyGame = "";

        //POUR OBTENIR LA DATE A L'INSTANT
        public static string DateNow()
        {
            DateTime heureCourante = DateTime.Now;    //Date a l'instant
            return heureCourante.ToString("HH:mm:ss");
        }

        //POUR MONTRER LES HISTORIQUES ET STATISTIQUE DES JOUEURS
        public static void ShowHistory()
        {
            for(int i=0; i<History.history.Length;i++)
            Console.WriteLine(History.history[i]);
            Console.ReadKey();
            Console.Clear();
        }
    }
}
 