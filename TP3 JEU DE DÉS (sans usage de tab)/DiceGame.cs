using System.Text;

namespace Dice_GameBody
{
    public static class DiceGame
    {
        public static int diceValue = 0;
        public static string DiceValues = "";
        public static string DiceValue = "";
        public static String[] ScorePlayers = new String[] { };
        public static String[] index = new String[] { };

        //DEROULEMNENT DU LANCEMENT DE DES
        public static void RollDice()
        {
            StringBuilder res = new StringBuilder();

            String chance = "123456";
            Random random = new Random();
            String[] cas = new String[] {"|   |\n| * |\n|   |","|*  |\n|   |\n|  *|","|*  |\n| * |\n|  *|",
                                         "|* *|\n|   |\n|* *|","|* *|\n| * |\n|* *|","|* *|\n|* *|\n|* *|"};

            diceValue = random.Next(1, 7);
            res.Append(chance[diceValue - 1]);
            DiceValue = res.ToString();
            Console.WriteLine(cas[diceValue - 1]);
        }

        //POUR INCREMENTER LE SCORE EN 2 POINTS
        public static void DoubleScore(int nbPlayers)
        {
            int myIndex = int.Parse(index[0]);
            int score = int.Parse(ScorePlayers[myIndex]);
            score = score + 2;
            ScorePlayers[myIndex] = score.ToString();
        }

        //POUR INCREMENTER LE SCORE EN 1 POINT
        public static void SimpleScore(int nbPlayers, int k)
        {
            for (int l = 0; l < k; l++)
            {
                int myIndex = int.Parse(index[l]);
                int score = int.Parse(ScorePlayers[myIndex]);
                score = score + 1;
                ScorePlayers[myIndex] = score.ToString();
            }
        }
    }
}
