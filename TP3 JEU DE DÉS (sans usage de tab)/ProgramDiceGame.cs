﻿using Dice_GameBody;
using HistoryGame;

namespace Dice_Game;
class ProgramDiceGame
{
    public static void MainDice()
    {
        //DEMANDER A L'USER LE NOMBRE DE JOUEURS
        Console.Clear();
        Console.Write("🎲🎲🎲🎲 DICE GAME 🎲🎲🎲🎲"+
                      "\n\nHow many players will play in this game? 🤔 : ");

        bool success  = Int32.TryParse(Console.ReadLine() ?? "", out int nbPlayers);
        while(success == false)
        {
            Console.Write("!!!😕How many players will(insert correctly)? 🤔 : ");
            success  = int.TryParse(Console.ReadLine() ?? "", out nbPlayers);
        }

        string Players = "";
        string Score = "";

        Console.WriteLine("\n");

        //ENTRER LES DIFFERENTS JOUEURS

        for (int i = 0; i < nbPlayers ; i++)
        {
            Console.Write($"Name's player 🧒 N{i + 1}: ");
            string name = Console.ReadLine() ?? "";
            while(name=="")
            {
                Console.Write($"!!!😕 Please insert the name's players 🧒 N{i + 1} :");
                name = Console.ReadLine()??"";
            }
            if(i==(nbPlayers-1))
            {
                Players+= name;
                Score+= 0;
            }
            else
            {
                Players+= name+";";
                Score+= 0+";";
            }
        }
        String[] namePlayers = Players.Split(';');
        DiceGame.ScorePlayers = Score.Split(';');
        
        int option = 1;

        //CORPS DU JEU------------------------------------------------------------------------------------- 

        while(option!=3)
        {   
            Console.ReadKey();
            Console.Clear();         
            Console.WriteLine("🎲🎲🎲🎲 DICE GAME 🎲🎲🎲🎲"+
                              "\n\nCurrent Score: \n");
            for(int i=0; i<nbPlayers; i++)
            {
                Console.WriteLine($"🎯 {namePlayers[i]} : {DiceGame.ScorePlayers[i]} points");
            }
            Console.WriteLine ("\n💎💎Choose one option :\n\n"+
                                "1 - ✨ New game\n" +
                                "2 - ✨ Show statistic games (History)\n"+
                                "3 - ✨ Exit\n"+ 
                                "\nWhat is your option ?");
            bool successConversion = Int32.TryParse(Console.ReadLine(),out option);
            while(successConversion == false)
            {
                Console.Write("!!!😕PLease insert correctly the option's number: ");
                successConversion = Int32.TryParse(Console.ReadLine(),out option);
            }
            Console.Clear();

            //NEW GAME -------------------------------------------------------------------------------------

            if(option == 1)
            {
                History.historyGame += "BEGIN : "+History.DateNow()+"\n\n";//ENREGISTRER LA DATE DU DEBUT

                Console.Write("🎲How many times you will Roll the Dice? 🤔 ");
                bool agree = Int32.TryParse(Console.ReadLine(), out int nbTime);
                while(agree == false)
                {
                    Console.Write("!!🎲How many times you will roll the Dice (insert correctly)? 🤔 ");
                    agree = Int32.TryParse(Console.ReadLine(), out nbTime);
                }
                  
                while(nbTime>0){
                    int count = 0;
                    string? temp = null;
                    int i = 0;
                    Console.ReadKey();
                    Console.Clear();
                    for (i = 0; i< nbPlayers ; i++)
                    {
                        Console.Write($"🧒 {namePlayers[i]}'s turn -"+
                                       "Would-you like to Roll the DICE ? 🤔(y or n) ");
                        
                        String choice = Console.ReadLine()??"";
                        if(choice == "y")
                        {
                            DiceGame.RollDice();
                             if (DiceGame.diceValue == 6)
                            {
                                temp += i + ";";
                                DiceGame.index = temp.Split(';');
                                count++;
                            }
                        }
                        DiceGame.DiceValues += DiceGame.DiceValue; 
                    }
                    
                    int k = DiceGame.index.Length-1;
                    if (k==1)
                    {
                        DiceGame.DoubleScore(nbPlayers);
                    }
                    else if(k>1)
                    {
                        DiceGame.SimpleScore(nbPlayers,k);
                    }
                    DiceGame.DiceValues= "";
                    DiceGame.index = new String[]{};
                    nbTime--;
                }

                for(int i=0; i<nbPlayers; i++)
                    History.historyGame += $"🏅{namePlayers[i]} : 🎯 {DiceGame.ScorePlayers[i]} points" + ';';
                
                History.historyGame += "\nFINAL : "+History.DateNow()+"\n"; //ENREGISTRER LA DATE DU FIN DE JEU

                History.history = History.historyGame.Split(';'); //ENREGISTRE LES SCORES DU MANCHE
            }
            
            //MONTRER LES HISTORIQUE DE PARTIE --------------------------------------------------------------------

            else if(option == 2)
                History.ShowHistory();

            //QUITTER LE JEU ---------------------------------------------------------------------------------------

            else if(option ==3)
                break;
        } 
    }
}
