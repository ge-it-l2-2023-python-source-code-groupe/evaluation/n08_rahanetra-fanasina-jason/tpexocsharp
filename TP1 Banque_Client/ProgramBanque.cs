﻿using ClassClient;
using ClassCompte;

namespace Banque
{
    class ProgramBanque
    {
        public static void MainBanque()
        {
            Console.Clear();
            Console.WriteLine("💱💱 Bienvenu dans Gestion Compte_Client 💱💱\n");

            string choice="y";
            int i=0;
            List<Compte> ListCompte = new List<Compte>();

            while (choice =="y")
            {

                Console.WriteLine("Voulez-vous créer votre compte (y or n) : ");
                choice = Console.ReadLine()?? "";
                while (choice!="y" && choice!="n")
                {
                    Console.WriteLine("!!😕Voulez-vous créer votre compte (y or n) : ");
                    choice = Console.ReadLine()?? "";
                }
                i ++;
                if (choice == "y")
                {
                    Console.WriteLine($"Compte {i}");
                    Console.WriteLine("Veuillez remplir les détails suivants :\n");

                    Console.Write("Entrer votre cin: ");
                    string cin = Console.ReadLine()?? "";
                    while(cin == "")
                    {
                        Console.Write("!!😕Entrer votre cin: ");
                        cin = Console.ReadLine()??"";
                    }

                    Console.Write("Entrer votre nom : ");
                    string nom = Console.ReadLine()?? "";
                    while(nom == "")
                    {
                        Console.Write("!!😕Entrer votre nom : ");
                        nom = Console.ReadLine()??"";
                    }

                    Console.Write("Entrer votre prénom : ");
                    string prenom = Console.ReadLine()?? "";
                    while(prenom == "")
                    {
                        Console.Write("!!😕Entrer votre nom : ");
                        prenom = Console.ReadLine()??"";
                    }

                    Console.Write("Entrer votre numéros de téléphone : ");
                    string num = Console.ReadLine()?? "";
                    while(num == "")
                    {
                        Console.Write("!!😕Entrer votre num : ");
                        num = Console.ReadLine()?? "";
                    }

                    Console.WriteLine("************************************");

                    //CREATION DU COMPTE----------------------------------------------------------------------------

                    Client client1 = new Client (cin,nom,prenom,num);
                    Compte compte1 = new Compte (client1);

                    ListCompte.Add(compte1);

                    Console.WriteLine("✨✨Votre compte a ete creer✨✨");
                    compte1.Afficher();

                    Console.WriteLine("************************************");
                    Console.WriteLine("Donner le montant à déposer : ");
                    int somme= 0;
                    string input = Console.ReadLine()?? "";
                    try{
                        somme = int.Parse(input);
                    }
                    catch(FormatException){
                        Console.WriteLine("Erreur de format.");
                    }
                    Console.WriteLine("Opération bien effectuée");
                    Console.WriteLine("************************************");
                    compte1.Crediter(somme);
                    compte1.Afficher();
                    Console.WriteLine("Donner le montant à rétirer : ");
                    string input1 = Console.ReadLine()?? "";
                    try{
                       somme = int.Parse(input1);
                    }
                    catch(FormatException){
                        Console.WriteLine("Erreur de format.");
                    }
                    Console.WriteLine("Opération bien effectuée");
                    Console.WriteLine("************************************");
                    compte1.Debiter(somme);
                    compte1.Afficher();
                }
                else 
                {
                    break;
                }
            }

            Console.WriteLine($"Le nombre de comptes créés : {i-1}");

            //TESTER LES METHODES Crediter(int somme, Compte debiteur) ET Debiter(int somme, Compte crediteur)

            if(i>1)
            {
                Console.Write("Souhaitez-vous CREDITER un compte a partir d'un autre compte 🤔 (y/n) ?");
                string option = Console.ReadLine()??"";

                while (option!="y" && option!="n")
                {
                    Console.Write("!!😕Souhaitez-vous CREDITER un compte a partir d'un autre compte 🤔 (y/n) ? ");
                    option = Console.ReadLine()?? "";
                }
                if(option == "y")
                {
                    int numCrediter = 0, numCrediteur=0, somme = 0;
                    bool success= false,a = false,b = false;
                    while(success!=true)
                    {
                        Console.Write("Entrer le numeros du compte a crediter : ");
                        success = int.TryParse(Console.ReadLine(),out numCrediter);
                    }
                    while(a!=true)
                    {
                        Console.Write("Entrer le numeros du compte debiteur : ");
                        a = int.TryParse(Console.ReadLine(),out numCrediteur);
                    }
                    while(b!=true)
                    {
                        Console.Write("Entrer le somme a crediter : ");
                        b = int.TryParse(Console.ReadLine(),out somme);
                    }                  
                    ListCompte[numCrediter-1].Crediter(somme, ListCompte[numCrediteur-1]);
                    Console.WriteLine($"✨✨Le Compte{numCrediter} a ete CREDITER par le Compte{numCrediteur} ✨✨");
                    foreach (Compte compte in ListCompte)
                    {
                        compte.Afficher();
                    }
                }
                Console.Write("Souhaitez-vous DEBITER un compte a partir d'un autre compte 🤔 (y/n) ?");
                option = Console.ReadLine()??"";

                while (option!="y" && option!="n")
                {
                    Console.Write("!!😕Souhaitez-vous DEBITER un compte a partir d'un autre compte 🤔 (y/n) ? ");
                    option = Console.ReadLine()?? "";
                }
                if(option == "y")
                {
                    int numDebite = 0, numCrediteur=0, somme = 0;
                    bool success= false,a = false,b = false;
                    while(success!=true)
                    {
                        Console.Write("Entrer le numeros du compte a debiter : ");
                        success = int.TryParse(Console.ReadLine(),out numDebite);
                    }
                    while(a!=true)
                    {
                        Console.Write("Entrer le numeros du compte a credite : ");
                        a = int.TryParse(Console.ReadLine(),out numCrediteur);
                    }
                    while(b!=true)
                    {
                        Console.Write("Entrer le somme a debiter : ");
                        b = int.TryParse(Console.ReadLine(),out somme);
                    }                  
                    ListCompte[numDebite-1].Debiter(somme, ListCompte[numCrediteur-1]);
                    Console.WriteLine($"✨✨ Compte{numDebite} a ete DEBITE par le Compte{numCrediteur} ✨✨");
                    foreach (Compte compte in ListCompte)
                    {
                        compte.Afficher();
                    }
                }
            }
        }
    }
}

