﻿using classPoint;
using classCercle;

namespace TP2;

class ProgramCercle
{
    public static void MainCercle()
    {
        Console.Clear();
        Console.WriteLine("⭕⭕ Bienvenu dans TP CERCLE ⭕⭕\n");
        double x = 0, y = 0, rayon = 0, a = 0, b = 0;
        bool success = false;

        while (success != true)
        {
            Console.Write("Donner l'abscisse du centre : ");
            success = double.TryParse(Console.ReadLine(), out x);
        }

        success = false;
        while (success != true)
        {
            Console.Write("Donner le coordonné du centre : ");
            success = double.TryParse(Console.ReadLine(), out y);
        }

        success = false;
        while (success != true)
        {
            Console.Write("Donner le rayon : ");
            success = double.TryParse(Console.ReadLine(), out rayon);
        }

        Point point1 = new Point(x, y);
        Cercle cercle = new Cercle(point1, rayon);
        cercle.Display();
        cercle.getPerimeter();
        cercle.getSurface();
        cercle.afficher();

        success = false;
        while (success != true)
        {
            Console.WriteLine("Donner un point : \n");
            Console.Write("X: ");
            success = double.TryParse(Console.ReadLine(), out a);
        }

        success = false;
        while (success != true)
        {
            Console.Write("Y: ");
            success = double.TryParse(Console.ReadLine(), out b);
        }


        Point p = new Point(a, b);
        p.Display();
        cercle.IsInclude(p);
    }
}
