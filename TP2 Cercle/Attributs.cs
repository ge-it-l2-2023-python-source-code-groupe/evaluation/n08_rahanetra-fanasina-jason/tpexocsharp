using classPoint;
namespace classCercle;
partial class Cercle
{
    public double Perimeter{get; set;}
    public double Surface{get; set;}
    public Point indexPoint{get; set;}
    public double R {get; set;}

    public Cercle(Point point1,double r)
    {
        indexPoint = point1;
        R = r;
    }
   
}