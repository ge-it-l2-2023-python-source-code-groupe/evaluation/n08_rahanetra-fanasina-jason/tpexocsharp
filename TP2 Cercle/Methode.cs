using classPoint;
namespace classCercle;
partial class Cercle
{
     public void Display()
    {
        Console.WriteLine($"CERCLE({indexPoint.X},{indexPoint.Y},{R})");
    }
    public double getPerimeter(){
        return Perimeter = 2*R*Math.PI;
    }

    public double getSurface(){
        return Surface = Math.PI* (R*R);
    }
    public void afficher(){
        Console.WriteLine($"Le perimetre du cercle est : {Perimeter:F2} \nLa surface du cercle est : {Surface:F2}");
    }

    public void IsInclude(Point p){
        double index = Math.Sqrt((indexPoint.X-p.X)*(indexPoint.X-p.X)+(indexPoint.Y-p.Y)*(indexPoint.Y-p.Y));

        if (R >= index){
            Console.WriteLine($"Le point appartient au cercle \n {index}");
        }
        else{
            Console.WriteLine(index);
            Console.WriteLine($"Le point n'appartient pas au cercle\n {index}");
        }
        
    }
}