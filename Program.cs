﻿using System;
using System.Collections;
using System.Diagnostics;
using Banque;
using Horloge_module;
using TP2;
using Dice_Game;
namespace TpExoCsharp;

class Program
{
    static void Main(string[] args)
    {
        string option = "1";
        while (option != "5")
        {
            Console.Clear();

            //MENU DE L'APPLICATION

            Console.WriteLine("💎💎💎💎 Bienvenu dans TP_APPLICATION 💎💎💎💎" +
                            "\n\n1 - 💲💲 TP1 (Banque-Client)  " +
                            "\n2 - ⭕⭕ TP2 (Cercle) " +
                            "\n3 - 🎲🎲 TP3 (Jeu de Dé)" +
                            "\n4 - ⏰⏰ TP4 (Horloge module)" +
                            "\n5 - Quitter\n" +
                            "\nQuelle est votre choix ? ");
            option = Console.ReadLine() ?? "";

            switch (option)
            {
                case "1":
                    Console.ReadKey();
                    Console.Clear();

                    //APPELLER LA METHODE MainBanque POUR LANCER DE CE TP GESTION BANQUE

                    Banque.ProgramBanque.MainBanque();
                    break;
                case "2":
                    Console.ReadKey();
                    Console.Clear();

                    //APPELLER LA METHODE MainBanque POUR LANCER CE TP CERCLE

                    TP2.ProgramCercle.MainCercle();
                    break;
                case "3":
                    Console.ReadKey();
                    Console.Clear();

                    //APPELLER LA METHODE MainBanque POUR LANCER DE CE TP JEU DE DÉS

                    Dice_Game.ProgramDiceGame.MainDice();
                    break;
                case "4":
                    Console.ReadKey();
                    Console.Clear();

                    //APPELLER LA METHODE MainBanque POUR LANCER DE CE TP HORLOGE

                    Horloge_module.ProgramHorloge.MainHorloge();
                    break;
                case "5":
                    Console.WriteLine("🎄🎄🎅Bye et joyeux noël🎅🎄🎄");
                    break;
            }


        }


    }
}
